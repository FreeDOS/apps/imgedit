# imgedit

Simple graphics editor for the Danger Engine

### License

BSD 3-Clause License
Copyright (c) 2021, Jerome Shidel
All rights reserved.

### Requirements

To build ImgEdit you will need the following...

* [FreeDOS](http://freedos.org/download/) version 1.3-RC5 or later
* [V8Power Tools for DOS](https://fd.lod.bz/repos/current/pkg-html/v8power.html)
21.07.14 or newer. It is installed automatically with FreeDOS.
* [UPX](https://upx.github.io) v3.96 or newer, it is included but not normally
installed with FreeDOS
* [PGME's QRESFILE](https://fd.lod.bz/repos/current/pkg-html/pgme.html) from
PGME v2021-08-20 or later, it is also included but not normally installed
with FreeDOS
* [NASM](https://nasm.us) 2.14.01 or later, also provided but not normally
installed with FreeDOS
* Turbo Pascal 7.0 ([version 5.5](https://cc.embarcadero.com/Item/26015) is
free and might work, but is untested and requires changing the the *ASM* files
to use *TP55.INC* instead of *TP70.INC*. And there are probably numerous compatibility issues)
* [Danger Engine](https://gitlab.com/DOSx86/sge) source files either
in main directory under **DANGER** or the parent directory **..\DANGER**
* [Danger fonts](https://github.com/shidel/fd-nls/tree/master/danger) placed
under both **IMGEDIT\FONTS** and **IMGVIEW\FONTS**
* [ImgEdit translations](https://github.com/shidel/fd-nls/tree/master/imgedit/imgedit)
placed in the **IMGEDIT** subdirectory
* [ImgView translations](https://github.com/shidel/fd-nls/tree/master/imgedit/imgview)
placed in the **IMGVIEW** subdirectory
