@echo off

if "%1" == "RES-SUB" goto RES-SUB

if "%_CWD%" == "" goto NoFreeDOS
set TEST_CWD=%_CWD%
pushd ..
if "%_CWD%" == "%TEST_CWD%" goto NoFreeDOS
popd
if not "%_CWD%" == "%TEST_CWD%" goto NoFreeDOS
set TEST_CWD=

set BUILD_APPS=%0

set BUILD_MODE=
if /I "%1" == "clean" set BUILD_MODE=clean
if /I "%1" == "dev" set BUILD_MODE=dev

if "%1" == "dev" goto NoUPXDev
if "%1" == "noupx" goto NoUPX
if "%1" == "upx" goto UseUPX
goto DoneSetUPX
:UseUPX
set USEUPX=yes
shift
goto DoneSetUPX
:NoUPX
shift
:NoUPXDev
set USEUPX=no
:DoneSetUPX

:Retry
set QRES=%DOSDIR%\LINKS\QRESFILE.BAT

if not exist %QRES% goto NoQRES

set ASM=NASM.EXE
set TPC=TPC.EXE
set QRES=CALL QRESFILE.BAT
if exist %DOSDIR%\LINKS\NASM.BAT set ASM=call NASM.BAT
if exist %DOSDIR%\LINKS\NASM.COM set ASM=NASM.COM
if exist %DOSDIR%\LINKS\TPC.BAT set TPC=call TPC.BAT
if exist %DOSDIR%\LINKS\TPC.COM set TPC=TPC.COM
if "%DANGER%" == "" set DANGER=.
if not exist %DANGER%\DANGER.PAS set DANGER=DANGER
if not exist %DANGER%\DANGER.PAS set DANGER=..\DANGER
if not exist %DANGER%\DANGER.PAS goto NoDanger

if "%BUILD_MODE%" == "clean" goto CLEAN
if "%BUILD_MODE%" == "dev" goto BUILD-DEV
if "%BUILD_MODE%" == "retry" goto BUILD
if not "%1" == "" goto %1
goto Build

:NoFreeDOS
echo Build script requires command shell features not present. Those features
echo are provided by FreeCOM the default shell for the FreeDOS operating system.
goto Error

:NoQRES
echo QRESFILE link not found.
goto Error

:NoDanger
echo Danger Engine Source not found.
goto Error

:CLEAN
pushd %DANGER%
call build.bat clean
popd
deltree /Y DANGER.DEF *.EXE *.TPU *.OBJ *.DAT *.LOG FAILED>NUL
goto end

:BUILD-DEV
deltree /Y *.TPU *.OBJ>NUL
pushd %DANGER%
deltree /Y *.TPU FAILED>NUL
popd
set BUILD_MODE=retry
:BUILD
deltree /Y *.EXE *.DAT *.LOG FAILED>NUL
if exist DANGER.TPU goto HaveDanger
if exist %DANGER%\DANGER.TPU goto CopyDanger
pushd %DANGER%
call build.bat
popd
if exist %DANGER%\DANGER.TPU goto Retry
goto End
:CopyDanger
copy /y %DANGER%\*.DEF .
copy /y %DANGER%\*.TPU .

:HaveDanger
set BUILD_MODE=
veach /d *.pas /x %BUILD_APPS% TPC-SUB *
if exist FAILED goto Error
veach /d *.exe /x %BUILD_APPS% ADD-RES *

if not "%USEUPX%" == "yes" goto SkipUPX
vecho /p Compress executables with /fCyan UPX /fGray /p
upx --8086 *.EXE
goto Done
:SkipUPX
if exist FAILED goto Error
if "%USEUPX%" == "yes" goto NoUPXWarning
:UPXWarning
vecho /p /fYellow Warning: /fGray Not all ( /s- /fDarkGray "but many" /fGray /n
vecho ) DOS platforms ( /s- /fDarkGray "like MS-DOS & PC-DOS" /n
vecho ) have issues /p when returning to the command prompt after executing /n
vecho /c32 the raw binaries. However, /p once they are compressed with /n
vecho /c32 /fLightGreen UPX /fGray /s- , /s+ those distributions /n
vecho /c32 no longer exhibit the /p problem.
:NoUPXWarning
vecho /p Binaries...
veach /d *.exe /d *.dat /x %BUILD_APPS% DIR-EXE *
vecho
goto Done

:TPC-SUB
if exist FAILED goto Error
if "%2" == "" goto Error
set PROG=
vfdutil /n %2 | set /p PROG=
if "%PROG%" == "" goto TPC-MAKE
if exist %PROG%.TPU goto End
if exist %PROG%.EXE goto End
vecho compile /fCyan %PROG%.PAS /fGray
%TPC% -M %PROG%.PAS
if errorlevel 1 goto Error-TPU
if exist %PROG%.TPU goto TPC-TPUOK
if exist %PROG%.EXE goto TPC-EXEOK
goto Error
:TPC-TPUOK
vecho created pascal unit /fLightGreen %PROG%.TPU /fGray
goto End
:TPC-EXEOK
vecho created executable /fWhite %PROG%.EXE /fGray
goto End

:DIR-EXE
dir %2 | vstr /b/l2
goto End

:RES-SUB
if "%2" == "FONTS" goto end
if "%2" == "DRIVERS" goto end
if not exist %2\NUL goto end
%QRES% /a /m /f %PROGD%\%PROG%.EXE /i %2\*.*
goto end

:ADD-RES
if "%2" == "" goto end
set PROG=
vfdutil /n %2 | set /p PROG=
if "%PROG%" == "" goto ADD-RES
if not exist %PROG%\NUL goto end
vecho attach /fCyan %PROG% /fGrey core resource assets
set PROGD="%_CWD%"
%QRES% /a /m /f %PROG%.EXE /i LICENSE
pushd %PROG%
%QRES% /a /m /f %PROGD%\%PROG%.EXE /i *.*
veach /a+ /d *.* /x %BUILD_APPS% RES-SUB *
if not exist FONTS\NUL goto NoFonts
pushd FONTS
%QRES% /a /m /f %PROGD%\%PROG%.EXE /i *.*
popd
:NoFonts
if not exist DRIVERSS\NUL goto NoDrivers
pushd DRIVERS
%QRES% /a /m /f %PROGD%\%PROG%.EXE /i *.*
popd
:NoDrivers
popd
pushd %DANGER%\DRIVERS
%QRES% /a /m /f %PROGD%\%PROG%.EXE /i *.DRV
popd
goto End

:Error-TPU
vecho /fLightRed FAILED /fGray %2

:Error
echo %1 %2 %3 %4 %5 %6 %7 %8 %9>>FAILED
verrlvl 1

:Done
set ASM=
set TPC=
set QRES=
set USEUPX=
set PROGD=
set PROG=
set DANGER=
set BUILD_APPS=
set BUILD_MODE=

:End
